// functions

function printStar(){

console.log("*")
console.log("**")
console.log("***")

}

printStar()

function sayHello(name){

	console.log('Hello' + name)
}

// sayHello('Zuitt')




// function that accepts two numbers and prints the sum
// Parameters (x and y)
function addSum(x,y){
	let sum = x + y
	console.log(sum)
}

// arguments
// addSum(13,2)
// addSum(23,56)


// function with 3 parameters
// String Template Literals
// Interpolation ${}
function printBio(lname, fname, age){
	// console.log('Hello' + lname+''+ fname+''+ age)
	console.log(`Hello ${lname} ${fname} ${age}`)
}

// console.log just prints on the console.


// printBio('Stark', 'Tony', 50)


// Return Keyword
function addSum(x,y){
	// console.log(x+y)
	return x+y
}

let sumNum = addSum(3,4)